import mysql.connector
import flask

import replicator


SERVER_PORT = 5001
SERVER_NAME = 'slave_n1'
SERVERS = {'master': 'http://127.0.0.1:5000'}
DB_CONFIG = {'user': 'root',
             'password': '123',
             'host': '127.0.0.1'}
DB_NAME = 'washer_n1'


# DATABASE
conn = mysql.connector.connect(**DB_CONFIG)
cursor = conn.cursor()
cursor.execute('USE {}'.format(DB_NAME))


# FLASK
app = flask.Flask(__name__)


@app.route('/', methods=['GET', 'POST'])
def api_root():
    result = []

    if flask.request.method == 'POST':
        if 'file' in flask.request.files:
            file = flask.request.files['file']
            commands = replicator.unpack_data(file)
            result = replicator.update_tables(conn=conn,
                                              cursor=cursor,
                                              commands=commands,
                                              server_name=flask.request.form.get('server_name'),
                                              servers=SERVERS)

    return replicator.pack_data(result)


if __name__ == '__main__':
    sender = replicator.SenderThread(conn=conn,
                                     cursor=cursor,
                                     server_name=SERVER_NAME,
                                     servers=SERVERS,
                                     timeout=1)
    sender.start()

    app.run(host='0.0.0.0', port=SERVER_PORT)
