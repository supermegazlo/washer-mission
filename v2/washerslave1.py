from threading import Thread
import time
import pickle
import requests
import flask
import mysql.connector


SERVER_PORT = 5001
WASHER_MASTER = 'http://127.0.0.1:5000'

DB_CONFIG = {'user': 'root',
             'password': '123',
             'host': '127.0.0.1'}
DB_NAME = 'washer_slave_1'


class SenderThread(Thread):

    def __init__(self):
        Thread.__init__(self)

        self.timeout = 1

        # MYSQL
        self._conn = mysql.connector.connect(**DB_CONFIG)
        self._cursor = self._conn.cursor()
        self._cursor.execute('USE {}'.format(DB_NAME))

    def run(self):
        while True:
            self.send_data()
            time.sleep(self.timeout)

    def send_data(self):
        updates = []

        # table cards
        query = """
                    SELECT * FROM tables_updates 
                    JOIN cards ON card_id = row_uid 
                    WHERE table_name = "cards"
                """
        self._cursor.execute(query)
        updates += self._cursor.fetchall()
        # table cards_operations
        query = """
                    SELECT * FROM tables_updates 
                    JOIN cards_operations ON card_operation_id = row_uid 
                    WHERE table_name = "cards_operations"               
                """
        self._cursor.execute(query)
        updates += self._cursor.fetchall()
        # table settings
        query = """
                    SELECT * FROM tables_updates 
                    JOIN settings ON setting_name = row_uid 
                    WHERE table_name = "settings"                   
                """
        self._cursor.execute(query)
        updates += self._cursor.fetchall()

        self._conn.commit()

        if len(updates) < 1:
            return

        try:
            r = requests.post(WASHER_MASTER,
                              files={'file': pickle.dumps(updates)},
                              data={})
            for row in pickle.loads(r.content):
                query = """
                            DELETE FROM tables_updates
                            WHERE table_name = %s AND row_uid = %s AND update_timestamp = %s
                        """
                self._cursor.execute(query, row[:2] + row[3:])
                self._conn.commit()

        except Exception as e:
            print(e)


app = flask.Flask(__name__)

cnx = mysql.connector.connect(**DB_CONFIG)
cursor = cnx.cursor()
cursor.execute('USE {}'.format(DB_NAME))


@app.route('/', methods=['GET', 'POST'])
def api_root():
    results = []

    if flask.request.method == 'POST':
        if 'file' in flask.request.files:
            file = flask.request.files['file']

            fields = {}

            for command in pickle.load(file):

                if command[0] not in fields:
                    query = 'SHOW COLUMNS FROM {}'.format(command[0])
                    cursor.execute(query)
                    fields[command[0]] = format_update(cursor.fetchall())

                if 'ins' in command[2]:
                    pass

                if 'upd' in command[2]:
                    query = """ 
                                UPDATE {} SET {};
                            """.format(command[0], fields[command[0]])
                    cursor.execute('SET @DISABLE_TRIGGERS=1')
                    result = cursor.execute(query,
                                            list(command[4:]) + [command[1]])
                    cursor.execute('SET @DISABLE_TRIGGERS=NULL')
                    cnx.commit()
                    results.append(command[:4])

    return pickle.dumps(results)


def format_update(fields):
    primary = ''
    values = []
    for field in fields:
        if field[3] == 'PRI':
            primary = field[0]
        values.append(field[0] + ' = %s')

    return ', '.join(values) + ' WHERE {} = %s'.format(primary)


if __name__ == '__main__':
    sender = SenderThread()
    sender.start()

    app.run(host='0.0.0.0', port=SERVER_PORT)


# SELECT COLUMN_NAME
#   FROM INFORMATION_SCHEMA.COLUMNS
#   WHERE TABLE_SCHEMA = 'washer_slave_1' AND TABLE_NAME = 'cards';

# @disable_triggers = true;
# // do the stuff that calls another triggers
# @disable_triggers = false;
