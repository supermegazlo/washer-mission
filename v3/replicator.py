import time
import pickle
import requests

from threading import Thread


def pack_data(data):
    return pickle.dumps(data)


def unpack_data(data):
    return pickle.load(data)


def unpack_file(file):
    return pickle.loads(data)


def update_tables(conn, cursor, commands):
    results = []
    fields = {}

    for command in commands:

        if command[0] not in fields:
            query = 'SHOW COLUMNS FROM {}'.format(command[0])
            cursor.execute(query)
            fields[command[0]] = cursor.fetchall()

        if 'INS' == command[2]:
            query = """
                        INSERT INTO {} VALUES({})
                    """.format(command[0], ', '.join(['%s'] * len(command[4:])))
            cursor.execute('SET @DISABLE_TRIGGERS=1')
            result = cursor.execute(query, command[4:])
            cursor.execute('SET @DISABLE_TRIGGERS=NULL')
            conn.commit()
            results.append(command[:4])

        if 'UPD' == command[2]:
            query = """ 
                        UPDATE {} SET {};
                    """.format(command[0], format_update(fields[command[0]]))
            cursor.execute('SET @DISABLE_TRIGGERS=1')
            result = cursor.execute(query, list(command[4:]) + [command[1]])
            cursor.execute('SET @DISABLE_TRIGGERS=NULL')
            conn.commit()
            results.append(command[:4])

        if 'DEL' == command[2]:
            query = """
                        DELETE FROM {} WHERE {} = %s
                    """.format(command[0], get_primary(fields[command[0]]))
            cursor.execute('SET @DISABLE_TRIGGERS=1')
            result = cursor.execute(query, [command[1]])
            cursor.execute('SET @DISABLE_TRIGGERS=NULL')
            conn.commit()
            results.append(command[:4])

    return results


def get_primary(fields):
    for field in fields:
        if field[3] == 'PRI':
            return field[0]


def format_insert(fields):
    pass


def format_update(fields):
    primary = ''
    values = []
    for field in fields:
        if field[3] == 'PRI':
            primary = field[0]
        values.append(field[0] + ' = %s')

    return ', '.join(values) + ' WHERE {} = %s'.format(primary)


class SenderThread(Thread):

    def __init__(self, conn, cursor, origin='master', servers=[], timeout=1):
        Thread.__init__(self)

        self._conn = conn
        self._cursor = cursor

        self._origin = origin
        self._servers = servers
        self._timeout = timeout

        if isinstance(self._servers, str):
            self._type = 'slave'
        else:
            self._type = 'master'

    def run(self):
        while True:
            self.send_data()
            time.sleep(self._timeout)

    def send_data(self):
        updates = self.check_updates()

        if len(updates) < 1:
            return

        try:
            if self._type == 'slave':
                r = requests.post(self._servers,
                                  files={'file': pack_data(updates)},
                                  data={'origin': self._origin})
            else:
                print(updates)
                return
            for row in unpack_file(r.content):
                query = """
                            DELETE FROM updates_queue
                            WHERE table_name = %s AND table_pk = %s AND update_timestamp = %s
                        """
                self._cursor.execute(query, row[:2] + row[3:])
                self._conn.commit()

        except Exception as e:
            print(e)

    def check_updates(self):
        updates = []

        # table cards
        query = """
                    SELECT * FROM updates_queue
                    JOIN cards ON card_id = table_pk
                    WHERE table_name = "cards"
                """
        self._cursor.execute(query)
        updates += self._cursor.fetchall()

        # table cards_operations
        query = """
                    SELECT * FROM updates_queue
                    JOIN cards_operations ON card_operation_id = table_pk
                    WHERE table_name = "cards_operations"
                """
        self._cursor.execute(query)
        updates += self._cursor.fetchall()

        # table settings
        query = """
                    SELECT * FROM updates_queue
                    JOIN settings ON setting_name = table_pk
                    WHERE table_name = "settings"
                """
        self._cursor.execute(query)
        updates += self._cursor.fetchall()

        # table delete
        query = """
                    SELECT * FROM updates_queue
                    WHERE update_type = "DEL"
                """
        self._cursor.execute(query)
        updates += self._cursor.fetchall()

        self._conn.commit()

        return updates
