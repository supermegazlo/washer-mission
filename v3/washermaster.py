import flask
import mysql.connector

import replicator


SERVER_PORT = 5000
ORIGIN = 'master'
SERVERS = {
    'group_n1': {'slave_n1': '127.0.0.1:5001',
                 'slave_n2': '127.0.0.1:5002'},
    'group_n2': {'slave_n3': '127.0.0.1:5003'}
}

DB_CONFIG = {'user': 'root',
             'password': '123',
             'host': '127.0.0.1'}
DB_NAME = 'washer_master'


conn = mysql.connector.connect(**DB_CONFIG)
cursor = conn.cursor()
cursor.execute('USE {}'.format(DB_NAME))

app = flask.Flask(__name__)


@app.route('/', methods=['GET', 'POST'])
def api_root():
    result = []

    if flask.request.method == 'POST':
        if 'file' in flask.request.files:
            file = flask.request.files['file']
            commands = replicator.unpack_data(file)
            result = replicator.update_tables(conn=conn,
                                              cursor=cursor,
                                              commands=commands,
                                              slave=flask.request.form.get('origin'))

    return replicator.pack_data(result)


if __name__ == '__main__':
    sender = replicator.SenderThread(conn=conn,
                                     cursor=cursor,
                                     origin=ORIGIN,
                                     servers=SERVERS,
                                     timeout=1)
    sender.start()

    app.run(host='0.0.0.0', port=SERVER_PORT)
