import time
import pickle
import requests

from threading import Thread


def pack_data(data):
    return pickle.dumps(data)


def unpack_data(data):
    return pickle.load(data)


def unpack_file(file):
    return pickle.loads(file)


def update_tables(conn, cursor, commands, server_name='master', servers={}):
    results = []
    fields = {}

    for command in commands:

        if command[0] not in fields:
            query = 'SHOW COLUMNS FROM {}'.format(command[0])
            cursor.execute(query)
            fields[command[0]] = cursor.fetchall()

        if 'INS' == command[2]:
            if server_name != 'master':
                if command[0] == 'settings':
                    command = command + (server_name, )
                else:
                    command = command + (find_group_by_slave(server_name, servers), )
            query = """
                        INSERT INTO {} VALUES({})
                    """.format(command[0], ', '.join(['%s'] * len(command[4:])))
            cursor.execute('SET @DISABLE_TRIGGERS=1')
            result = cursor.execute(query, command[4:])
            cursor.execute('SET @DISABLE_TRIGGERS=NULL')
            conn.commit()
            results.append(command[:4])

        if 'UPD' == command[2]:
            if server_name != 'master':
                if command[0] == 'settings':
                    command = command + (server_name, )
                else:
                    command = command + (find_group_by_slave(server_name, servers), )
            query = """ 
                        UPDATE {} SET {};
                    """.format(command[0], format_update(fields[command[0]]))
            cursor.execute('SET @DISABLE_TRIGGERS=1')
            result = cursor.execute(query, list(command[4:]) + [command[1]])
            cursor.execute('SET @DISABLE_TRIGGERS=NULL')
            conn.commit()
            results.append(command[:4])

        if 'DEL' == command[2]:
            query = """
                        DELETE FROM {} WHERE {} = %s
                    """.format(command[0], get_primary(fields[command[0]]))
            cursor.execute('SET @DISABLE_TRIGGERS=1')
            result = cursor.execute(query, [command[1]])
            cursor.execute('SET @DISABLE_TRIGGERS=NULL')
            conn.commit()
            results.append(command[:4])

    return results


def get_primary(fields):
    for field in fields:
        if field[3] == 'PRI':
            return field[0]


def format_insert(fields):
    pass


def format_update(fields):
    primary = ''
    values = []
    for field in fields:
        if field[3] == 'PRI':
            primary = field[0]
        values.append(field[0] + ' = %s')

    return ', '.join(values) + ' WHERE {} = %s'.format(primary)


def find_slave_by_servers(slave, servers):
    for group in servers.values():
        if slave in group:
            return group[slave]


def find_group_by_slave(slave, servers):
    for group in servers:
        if slave in servers[group]:
            return group


class SenderThread(Thread):

    def __init__(self, conn, cursor, server_name='master', servers={}, timeout=1):
        Thread.__init__(self)

        self._conn = conn
        self._cursor = cursor

        if server_name == 'master':
            self._server_name = 'master'
        else:
            self._server_name = server_name

        self._servers = servers
        self._timeout = timeout

    def run(self):
        while True:
            self.send_data()
            time.sleep(self._timeout)

    def send_data(self):
        updates = self.check_updates()

        if len(updates) < 1:
            return

        if self._server_name == 'master':
            self.master_updates()
            updates = self.updates_group_by_slave(updates)
            for slave_server in updates:
                try:
                    r = requests.post(find_slave_by_servers(slave_server, self._servers),
                                      files={'file': pack_data(
                                          updates[slave_server])},
                                      data={'server_name': self._server_name})
                    for row in unpack_file(r.content):
                        query = """
                                    DELETE FROM updates_queue
                                    WHERE table_name = %s AND table_pk = %s AND update_type = %s AND update_timestamp = %s AND update_server = %s
                                """
                        self._cursor.execute(query, row + (slave_server, ))
                        self._conn.commit()
                except Exception as e:
                    print(e)

        else:
            try:
                r = requests.post(self._servers['master'],
                                  files={'file': pack_data(updates)},
                                  data={'server_name': self._server_name})
                for row in unpack_file(r.content):
                    query = """
                                DELETE FROM updates_queue
                                WHERE table_name = %s AND table_pk = %s AND update_type = %s AND update_timestamp = %s
                            """
                    self._cursor.execute(query, row)
                    self._conn.commit()
            except Exception as e:
                print(e)

    def check_updates(self):
        updates = []

        # table cards
        query = """
                    SELECT * FROM updates_queue
                    JOIN cards ON card_id = table_pk
                    WHERE table_name = "cards"
                """
        self._cursor.execute(query)
        updates += self._cursor.fetchall()

        # table cards_operations
        query = """
                    SELECT * FROM updates_queue
                    JOIN cards_operations ON card_operation_id = table_pk
                    WHERE table_name = "cards_operations"
                """
        self._cursor.execute(query)
        updates += self._cursor.fetchall()

        # table settings
        query = """
                    SELECT * FROM updates_queue
                    JOIN settings ON setting_name = table_pk
                    WHERE table_name = "settings"
                """
        self._cursor.execute(query)
        updates += self._cursor.fetchall()

        # table delete
        query = """
                    SELECT * FROM updates_queue
                    WHERE update_type = "DEL"
                """
        self._cursor.execute(query)
        updates += self._cursor.fetchall()

        self._conn.commit()

        return updates

    def master_updates(self):
        query = """
                    SELECT * FROM updates_queue
                    WHERE update_server = ""
                """
        self._cursor.execute(query)
        rows = self._cursor.fetchall()
        for row in rows:
            if row[0] == 'settings':
                query = "SELECT slave_server FROM settings WHERE setting_name = %s"
                self._cursor.execute(query, [row[1]])
                slave_server = self._cursor.fetchone()[0]
                query = """
                            REPLACE updates_queue
                            VALUES(%s, %s, %s, %s, %s)
                        """
                row_insert = list(row)
                row_insert[4] = slave_server
                self._cursor.execute(query, row_insert)
                query = """
                            DELETE FROM updates_queue
                            WHERE table_name = %s AND table_pk = %s AND update_type = %s AND update_server = %s
                        """
                self._cursor.execute(query,
                                     [row[0], row[1], row[2], row[4]])

            if row[0] == 'cards':
                query = "SELECT slave_group FROM cards WHERE card_id = %s"
                self._cursor.execute(query, [row[1]])
                slave_group = self._cursor.fetchone()[0]
                if slave_group in self._servers:
                    for slave_server in self._servers[slave_group]:
                        query = """
                                    REPLACE updates_queue
                                    VALUES(%s, %s, %s, %s, %s)
                                """
                        row_insert = list(row)
                        row_insert[4] = slave_server
                        self._cursor.execute(query, row_insert)
                    query = """
                                DELETE FROM updates_queue
                                WHERE table_name = %s AND table_pk = %s AND update_type = %s AND update_server = %s
                            """
                    self._cursor.execute(query,
                                         [row[0], row[1], row[2], row[4]])

            if row[0] == 'cards_operations':
                query = "SELECT slave_group FROM cards WHERE cards_operations = %s"
                self._cursor.execute(query, [row[1]])
                slave_group = self._cursor.fetchone()[0]
                if slave_group in self._servers:
                    for slave_server in self._servers[slave_group]:
                        query = """
                                    REPLACE updates_queue
                                    VALUES(%s, %s, %s, %s, %s)
                                """
                        row_insert = list(row)
                        row_insert[4] = slave_server
                        self._cursor.execute(query, row_insert)
                    query = """
                                DELETE FROM updates_queue
                                WHERE table_name = %s AND table_pk = %s AND update_type = %s AND update_server = %s
                            """
                    self._cursor.execute(query,
                                         [row[0], row[1], row[2], row[4]])

        self._conn.commit()

    def updates_group_by_slave(self, updates):
        result = {}

        for update in updates:
            if update[4] not in result:
                result[update[4]] = []
            result[update[4]].append(update[:4] + update[5:-1])

        return result
