from threading import Thread
import time
import pickle
import requests
import flask
import mysql.connector


SERVER_PORT = 5001
SERVER_SECOND = 'http://127.0.0.1:5000/'
DB_NAME = 'logs_5001'
FILE_QUEUE = DB_NAME + '.queue'
REQUEST_TIMEOUT = 10


class ReplicatorQueue:

    def __init__(self, thread):
        self.thread = thread
        self.file = FILE_QUEUE
        self.queue = []

        self._load()

    def get(self):
        if len(self.queue) > 0:
            return self.queue[0]
        return None

    def pop(self):
        if len(self.queue) > 0:
            self.queue.pop(0)
            self._save()

    def put(self, value):
        self.thread.timeout = 0
        self.queue.append(value)
        self._save()

    def _load(self):
        try:
            with open(self.file, 'rb') as f:
                self.queue = pickle.load(f)
        except FileNotFoundError:
            pass

    def _save(self):
        with open(self.file, 'wb') as f:
            pickle.dump(self.queue, f)


class ReplicatorThread(Thread):

    def __init__(self):
        Thread.__init__(self)

        self.timeout = 0
        self.url = SERVER_SECOND
        self.queue = ReplicatorQueue(self)

    def run(self):
        while True:
            self.send_data()
            time.sleep(0.1)

    def send_data(self):
        args = self.queue.get()
        if args is not None:
            args['norepl'] = ''
            try:
                r = requests.get(self.url, params=args)
                print(r)
                if r.text == 'ok':
                    self.queue.pop()
            except Exception as e:
                print(e)

                # перерыв между запросами
                self.timeout = REQUEST_TIMEOUT
                while self.timeout > 0:
                    self.timeout -= 1
                    time.sleep(1)

### FLASK ###########################################################
app = flask.Flask(__name__)


@app.route('/')
def api_logger():
    args = flask.request.args

    timestamp = args.get('timestamp', '')
    message = args.get('message', '')
    text = args.get('text', '')

    try:
        if 'norepl' not in args:
            replicator.queue.put(args.to_dict())

        query = 'INSERT INTO logs SET log_timestamp=%s, log_message=%s, log_text=%s'
        cursor.execute(query, (timestamp, message, text))
        cnx.commit()
    except mysql.connector.Error as err:
        print('MYSQL ERORR: {}'.format(err))
        exit()

    return 'ok'


@app.route('/data')
def api_data():
    query = 'SELECT * FROM logs'
    cursor.execute(query)
    rows = cursor.fetchall()
    res = ''
    for row in rows:
        res += "<br>\n".join(map(str, row)) + "<br><br>\n"
    return res


### DB ##############################################################
DB_CONFIG = {'user': 'root',
             'password': '123',
             'host': '127.0.0.1'}

DB_CREATE_LOGS_DATABASE = """
  CREATE DATABASE IF NOT EXISTS {} DEFAULT CHARACTER SET 'utf8'
""".format(DB_NAME)

DB_CREATE_LOGS_TABLE = """ CREATE TABLE IF NOT EXISTS logs (
  log_timestamp TIMESTAMP NOT NULL,
  log_message TEXT NOT NULL,
  log_text TEXT NOT NULL
  ) ENGINE=InnoDB
"""

cnx = mysql.connector.connect(**DB_CONFIG)
cursor = cnx.cursor()

try:
    cursor.execute(DB_CREATE_LOGS_DATABASE)
    cursor.execute('USE {}'.format(DB_NAME))
    cursor.execute(DB_CREATE_LOGS_TABLE)
except mysql.connector.Error as err:
    print('MYSQL ERORR: {}'.format(err))
    exit()


if __name__ == '__main__':
    replicator = ReplicatorThread()
    replicator.start()

    app.run(host='0.0.0.0', port=SERVER_PORT)

    cnx.close()
